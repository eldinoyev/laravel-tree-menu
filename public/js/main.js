/*global $:false,console:false*/
'use strict';
function TreeController($scope, $http) {

    /*$http.get('bladetree').success(function(nodes) {
        $scope.nodes = nodes;
        createDepths();
        console.log($scope.nodes);
    });*/
    $scope.initTree = function(){
        $http.get('tree').success(function(nodes) {
            console.log(nodes);
            $scope.nodes = nodes;
        });

        //JQuery Controls and Handlers =================================================
        $(document).ready(function(){
            console.log('ready');
            
            //Branch-sizing handler
            $scope.$apply(branchHandlers());
            
        });
        //===============================================================================
    };
}

    
