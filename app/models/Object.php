<?php
use Baum\Node;

/**
* Object
*/
class Object extends Baum\Node {

  /**
   * Table name.
   *
   * @var string
   */
  protected $table = 'objects';

}
