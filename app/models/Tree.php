<?php
use Jenssegers\Mongodb\Model as Eloquent;

/**
* Object
*/
class Tree extends Eloquent {

  /**
   * Table name.
   *
   * @var string
   */
  /*protected $table = 'objects';*/
  protected $collection = 'tree';

}
