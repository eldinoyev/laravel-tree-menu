<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Blade::setContentTags('<%', '%>');

/*Route::get('/', 'ObjectController@index');

Route::get('tree', 'ObjectController@tree');

Route::post('addto', 'ObjectController@addTo');

Route::post('deletenode', 'ObjectController@deleteNode');

Route::get('root', 'ObjectController@root');

Route::get('delete/{id}', function($id){
	$child = Object::find($id);
	$child->delete();
	return 'done';
});*/

Route::get('/', 'TreeController@index');
Route::get('tree', 'TreeController@tree');
Route::get('newtree', function(){ /*Example of creating new collection - Use to create collection for each new user*/
	Schema::create('users');
});