<!doctype html>
<h1ml>
	<head>
		<style type="text/css">
		div.node {
			display:block;
		}

		span.depth {

		}

		p {
			display:inline-block;
			margin:0;
		}
		</style>
	</head>
	<body>
		@foreach ($objects as $object)
			<div class="node">
			<span class="depth">
				@for ($i = 0; $i < $object->depth; $i++) &nbsp;|&nbsp; @endfor
			</span>
			<p>{{$object->name}}</p>&nbsp;<a href="/addto/{{$object->id}}">Add</a>
			</div>
		@endforeach
	</body>
</h1ml>