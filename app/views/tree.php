<!-- 
WHERE YOURE AT:
DATA FINALLY CAME THROUGH

https://github.com/jenssegers/Laravel-MongoDB

$ mongo
$ use enttree
$ db.tree.find()

$ php artisan serve

Figure out how to create or use a new collection for each user
Lookup how to use Laravel Schemas

Build working AngularJS Functionality around db.tree (db: enttree)
Recreate tree the way it was before
    -Figure out Tree structure
 -->
<!doctype html>
<html lang="en" ng-app>
<head>
    <meta charset="UTF-8">
    <title>Laravel-Tree-Menu</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
</head>

<body ng-controller="TreeController" ng-init="initTree()" >
    <div class="container main">
        <div class="row ">
            <div class="col-sm-3 tree">
            <!-- ====== -->
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Action <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <!--   ======    -->

                <button type="button" class="debug btn">Debug</button>
                <!-- ========================================== FAKE BRANCHES -->
                <!-- <div class="animate-wrap">
                <div class="branch">
                    <div class="child-viewer">V</div>
                    <div class="body">
                        <div class="branch-toggle"><div class="toggler active"></div></div>
                        <div class="branch-name"><p class="name">abaaadgadgadgaasfafafuiniuniunidfgadgadgaasfafafuiniuniunidfgadgadgaasfafafuiniuniunidfiuhndiuanhiufiudfiunafdignidufgadadgdgubgiuabd</p></div>
                    </div>

                </div>
                <div class="branch">
                    <div class="child-viewer">V</div>
                    <div class="body">
                        <div class="branch-toggle"><div class="toggler"></div></div>
                        <div class="branch-name"><p class="name">abaaabgiuabd</p></div>
                    </div>

                </div>
                </div><br/><br/> -->
                <!-- ========================================================= -->
                <div class="branch" ng-repeat="node in nodes">
                    <div class="child-viewer">V</div>
                    <div class="body">
                        <div class="branch-toggle" ng-click="toggler()"><div class="toggler"></div></div>
                        <div class="branch-name"><p class="name">{{node.name}}</p></div>
                    </div>
                </div>
                <br/>
                
            </div>
            <div class="col-sm-4 note-list">
                <!-- <input type="text" placeholder="Name of new node" ng-model="newNodeName">
                <div class="node" ng-repeat="node in nodes">
                    <a href="#" ng-click="addNode(node.id)">Add</a><a href="#" ng-click="deleteNode(node.id)">Delete</a><span class="depth"><span ng-repeat="level in node.depthArr">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>
                    <p>{{node.name}}</p>&nbsp;
                </div> -->
            </div>
            <div class="col-sm-5 note">
                &nbsp;
                <h1>Things I should probably be doing:</h1>
                <ul>
                    <li>Creating depth functionality for Branches</li>
                    <li>Adding the options button to branches</li>
                    <li>Adding random color to every selected Toggler</li>
                    <li>Displaying notes based on active notes: 
                        <ul>
                            <li>Create new data table</li>
                            <li>fill it with notes linking to a node id</li>
                        </ul>
                    </li>
                    <li>Renaming a bunch of stuff: 
                        <ul>
                            <li>Model names</li>
                            <li>Project name</li>
                            <li>Nodes, Objects, etc.</li>
                        </ul>
                    </li>
                    <li>Get project ready for GitHub</li>
                </ul>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
</body>
</html>
