<?php

class ObjectController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$objects = Object::all();
		return View::make('tree')->with('nodes', $objects);
	}

	public function tree()
	{
		return Object::all();

	}

	public function addTo()
	{
		$id = Input::get('id');
		$name = Input::get('name');
		$child = Object::create(['name' => $name]);
		$node = Object::find($id);
		$child->makeChildOf($node);
	}

	public function deleteNode()
	{
		$id = Input::get('id');
		$node = Object::find($id);
		$node->delete();	

	}

	public function root()
	{
		$tree = Object::all();
		$root = Object::where('depth', "=", 0)->first(); //getRoot() workaround
		return $root;
	}
}