<?php

class TreeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$nodes = Tree::all();
		return View::make('tree')->with('nodes', $nodes);
	}

	public function tree()
	{
		$nodes = Tree::all();
		return $nodes;
	}

}